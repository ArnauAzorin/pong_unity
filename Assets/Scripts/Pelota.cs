﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pelota : MonoBehaviour
{

    public float velY; 
    public float velX; 

    public float posy;

    public float posx;

    public float maxY;
    public float maxX;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
         posy = transform.position.y + velY*Time.deltaTime;

         posx = transform.position.x + velX*Time.deltaTime;
        
        if(posy>maxY)
        {
            posy = maxY;
            velY*=-1;
        }
        else if(posy<-maxY)
        {
            posy = -maxY;
            velY*=-1;
        }

        if(posx>maxX)
        {
            posx = maxX;
            velX*=-1;
        }
        else if(posx<-maxX)
        {
            posx = -maxX;
            velX*=-1;
        }

         transform.position = new Vector3(posx, posy, transform.position.z);
    }
}
