﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class BlockMuerte: Arkanoid_Block
{
  
    public GameObject killer;
 
    public override void TouchBall(){
        GameObject go = Instantiate(killer);
        go.transform.position = transform.position;
        Destroy(gameObject);
    }
}