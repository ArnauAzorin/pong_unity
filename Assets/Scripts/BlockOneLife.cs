﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class BlockOneLife : Arkanoid_Block
{
    protected override void Start(){
        lives = 1;
        Colorear();
    }

    protected override void Colorear(){
        SpriteRenderer sp = GetComponent<SpriteRenderer>();
        sp.color = new Color(1f,0f,0f,1f);
    }

}