﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arkanoid_Player : MonoBehaviour
{
    public float velocidadX;
    public float maxX;
    public float maxX2;
    private float posX;
    private float direction;

    public Transform ballPosition;

    // Update is called once per frame
    void Update()
    {
        //if(ballPosition.position.x >= 0){
            if(ballPosition.position.x>transform.position.x){
                direction = 1.0f;
            }else{
                direction = -1.0f;
            }
        //}else{
        //    direction = 0f;
        //}

        posX = transform.position.x + direction*velocidadX*Time.deltaTime;

        if(posX>maxX){
            posX = maxX;
        }else if(posX<maxX2){
            posX = maxX2;
        }

        transform.position = new Vector3(posX, transform.position.y, transform.position.z);       
    }
}
