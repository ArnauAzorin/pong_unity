﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class BlockTwoLife : Arkanoid_Block
{
    protected override void Start(){
        lives = 2;
        Colorear();
    }

    protected override void Colorear(){
        SpriteRenderer sp = GetComponent<SpriteRenderer>();
        sp.color = new Color(1f, 0.92f, 0.016f, 1f);
    }

}