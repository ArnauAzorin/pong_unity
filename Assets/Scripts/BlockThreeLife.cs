﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class BlockThreeLife : Arkanoid_Block
{
    
    protected override void Start(){
        lives = 3;
        
        Colorear();
    }

    protected override void Colorear(){
        SpriteRenderer sp = GetComponent<SpriteRenderer>();
        sp.color = new Color(0f, 1f, 0f, 1f);
    }

}